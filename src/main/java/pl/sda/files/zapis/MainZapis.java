package pl.sda.files.zapis;

import java.io.*;

public class MainZapis {
    public static void main(String[] args) {

//        try (PrintWriter writer = new PrintWriter("plik2.txt")){ // nadpisanie
                                                                    // niżej dopisanie
        try (PrintWriter writer = new PrintWriter(new FileWriter("plik2.txt", true))){

            writer.println("Hello World!");

        } catch (IOException e) {
            e.printStackTrace();
        }
//        PrintWriter writer = new PrintWriter(new File("plik.txt"));
    }
}

