package pl.sda.files.zapis.zad3;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        // ta implementacja na początku wyczyści plik
//        // raz otwarty plik
//        try (PrintWriter writer = new PrintWriter(new FileWriter("output_3.txt"))) {
//            String linia = "";
//            // wielokrotnie zapisuje do pliku
//            while (!linia.equals("quit")) {
//
//                linia = scanner.nextLine();
//                writer.println(linia);
//                writer.flush();
//                // czyszczę bufor między zapisami
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


        // ta implementacja nie wyczysci pliku przy uruchomieniu
        String linia = "";
        while (!linia.equals("quit")) {
            // wielokrotnie otwieram plik
            FileWriter writer1 = null;
            try {
                writer1 = new FileWriter("output_3.txt", true);

                try (PrintWriter writer = new PrintWriter(writer1)) {
                    linia = scanner.nextLine();
                    writer.println(linia);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}

