package pl.sda.files.zapis.zad4;

import java.io.File;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Scanner;

public class MainZadanie4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String sciezkaDoPliku = scanner.nextLine();

        File plik = new File(sciezkaDoPliku);

        if (plik.exists()) {
            System.out.println("Plik istnieje");

            if (plik.isFile()) {
                System.out.println("Jest plikiem");
            } else if (plik.isDirectory()) {
                System.out.println("Jest katalogiem");
            }

            System.out.println("Długość: " + plik.length());
            System.out.println("Czas modyfikacji: " + plik.lastModified());
            System.out.println("Prawa odczytu: " + plik.canRead());
            System.out.println("Prawa zapisu: " + plik.canWrite());

            Timestamp timestamp = new Timestamp(plik.lastModified());
            LocalDateTime localDateTime = timestamp.toLocalDateTime();
            System.out.println("Czas modyfikacji: " + localDateTime);
        } else {
            System.err.println("Plik nie istnieje!");
        }
    }
}
