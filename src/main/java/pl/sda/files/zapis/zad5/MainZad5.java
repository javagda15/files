package pl.sda.files.zapis.zad5;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class MainZad5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        String odpowiedz;
        do {
            System.out.println("Podaj wiek:");
            odpowiedz = scanner.nextLine();
            int wiek = Integer.parseInt(odpowiedz);

            System.out.println("Podaj wzrost:");
            odpowiedz = scanner.nextLine();
            int wzrost = Integer.parseInt(odpowiedz);

            System.out.println("Podaj płeć [Kobieta, Mężczyzna]:");
            odpowiedz = scanner.nextLine();
            boolean jestKobietą = odpowiedz.equalsIgnoreCase("kobieta");

            System.out.println("Podaj zarobki:");
            odpowiedz = scanner.nextLine();
            int zarobki = Integer.parseInt(odpowiedz);

            // stworzyłem obiekt formularza
            Formularz formularz = new Formularz(wiek, wzrost, zarobki, jestKobietą);

            // zapisałem formularz do pliku
            try (PrintWriter writer = new PrintWriter(new FileWriter("output_5.txt", true))) {
                writer.print(formularz.zamienNaTrescDoPliku());
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("Czy chcesz kontynuować? [yes, cokolwiek innego]");
            odpowiedz = scanner.nextLine();
        } while (odpowiedz.equalsIgnoreCase("yes"));
    }
}
