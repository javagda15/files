package pl.sda.files.zapis.zad5;

public class Formularz {
    private int wiek, wzrost, zarobki;
    private boolean jestKobieta;

    public Formularz(int wiek, int wzrost, int zarobki, boolean jestKobieta) {
        this.wiek = wiek;
        this.wzrost = wzrost;
        this.zarobki = zarobki;
        this.jestKobieta = jestKobieta;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public int getWzrost() {
        return wzrost;
    }

    public void setWzrost(int wzrost) {
        this.wzrost = wzrost;
    }

    public int getZarobki() {
        return zarobki;
    }

    public void setZarobki(int zarobki) {
        this.zarobki = zarobki;
    }

    public boolean isJestKobieta() {
        return jestKobieta;
    }

    public void setJestKobieta(boolean jestKobieta) {
        this.jestKobieta = jestKobieta;
    }

    public String zamienNaTrescDoPliku() {
        StringBuilder builder = new StringBuilder();
        builder.append("wiek=").append(wiek).append("\n");
        builder.append("wzrost=").append(wzrost).append("\n");
        builder.append("zarobki=").append(zarobki).append("\n");
        builder.append("jestkobieta=").append(jestKobieta).append("\n");
        return builder.toString();
    }
}
