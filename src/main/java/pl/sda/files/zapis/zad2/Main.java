package pl.sda.files.zapis.zad2;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String linia = scanner.nextLine();
        try (PrintWriter writer = new PrintWriter(new FileWriter("output_2.txt", true))) {
            writer.println(linia);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
