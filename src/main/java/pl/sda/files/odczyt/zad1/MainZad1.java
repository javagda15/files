package pl.sda.files.odczyt.zad1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MainZad1 {
    public static void main(String[] args) {
        if (!new File("output_1.txt").exists()) {
            System.err.println("Plik nie istnieje!");
        } else {
            try (BufferedReader reader = new BufferedReader(new FileReader("output_1.txt"))) {
                String linia = reader.readLine();

                System.out.println(linia);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
