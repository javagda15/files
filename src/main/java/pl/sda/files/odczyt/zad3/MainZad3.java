package pl.sda.files.odczyt.zad3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MainZad3 {
    public static void main(String[] args) {
        if (!new File("output_5.txt").exists()) {
            System.err.println("Plik nie istnieje!");
        } else {
            try (BufferedReader reader = new BufferedReader(new FileReader("output_5.txt"))) {
                String linia;
                int licznik = 0, licznikSlow = 0;

                while ((linia = reader.readLine()) != null) {
                    licznik++;
                    licznikSlow += linia.split(" ").length; // dodaj ilość słów w linii
                }

                System.out.println("Licznik: " + licznik);
                System.out.println("Licznik słów: " + licznikSlow);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
