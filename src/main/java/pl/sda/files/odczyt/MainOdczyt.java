package pl.sda.files.odczyt;


import java.io.*;
import java.util.Scanner;

public class MainOdczyt {
    public static void main(String[] args) {
        ///
        ///
//        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("output_5.txt"))) {
//            String linia = bufferedReader.readLine();
//
//            while (linia != null) {
//                System.out.println(linia);
//
//                linia = bufferedReader.readLine();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        try (BufferedReader reader = new BufferedReader(new FileReader("output_5.txt"))) {
            String linia;

            while ((linia = reader.readLine()) != null) {
                System.out.println(linia);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        File plik = new File("output_5.txt");
        if (plik.exists()) {
            try (Scanner scanner = new Scanner(plik)) {
                while (scanner.hasNextLine()) {
                    String linia = scanner.nextLine();
                    System.out.println(linia);
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }


    }
}
