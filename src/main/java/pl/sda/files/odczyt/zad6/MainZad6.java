package pl.sda.files.odczyt.zad6;

import pl.sda.files.zapis.zad5.Formularz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;

public class MainZad6 {
    public static void main(String[] args) {
        if (!new File("output_5.txt").exists()) {
            System.err.println("Plik nie istnieje!");
        } else {
            List<Formularz> list = new ArrayList<>();

            try (BufferedReader reader = new BufferedReader(new FileReader("output_5.txt"))) {
                String linia;
                while ((linia = reader.readLine()) != null && !linia.isEmpty()) {
                    // wiem że mam pierwszą linię formularza.
                    int wiek = Integer.parseInt(linia.split("=")[1]);
                    int wzrost = Integer.parseInt(reader.readLine().split("=")[1]);
                    int zarobki = Integer.parseInt(reader.readLine().split("=")[1]);
                    boolean jestKobietą = Boolean.parseBoolean(reader.readLine().split("=")[1]);

                    Formularz formularz = new Formularz(wiek, wzrost, zarobki, jestKobietą);
                    list.add(formularz);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("Ilość: " + list.size());

            OptionalDouble sredniWiek = list.stream()
                    .filter(f -> f.isJestKobieta())
                    .mapToInt(f -> f.getWiek())
                    .average();

            if (sredniWiek.isPresent()) {
                System.out.println("Średni wiek: " + sredniWiek.getAsDouble());
            }
        }
    }
}
