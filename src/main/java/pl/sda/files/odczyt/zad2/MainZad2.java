package pl.sda.files.odczyt.zad2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MainZad2 {
    public static void main(String[] args) {
        if (!new File("output_2.txt").exists()) {
            System.err.println("Plik nie istnieje!");
        } else {
            try (BufferedReader reader = new BufferedReader(new FileReader("output_2.txt"))) {
                String linia;
                while ((linia = reader.readLine()) != null) {
                    System.out.println(linia.toLowerCase());
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
