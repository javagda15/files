//package pl.sda.files.parser;
//
//import java.util.Map;
//import java.util.Optional;
//import java.util.TreeMap;
//
//public class Dictionary {
//    private Map<String, Translation> wordsPL_EN = new TreeMap<>();
//    private Map<String, Translation> wordsEN_PL = new TreeMap<>();
//
//    public void addWord(String polishWord, String... translations) {
//        polishWord = polishWord.toLowerCase();
//        for (int i = 0; i < translations.length; i++) {
//            translations[i] = translations[i].toLowerCase();
//        }
//
//        if (wordsPL_EN.containsKey(polishWord)) {
//            System.out.println("This word already exists. Overwriting...");
//        }
//
////        Translation translation = new Translation(
////                polishWord,
////                translations[0],
////                translations.length > 1 ? translations[1] : null);
//
//        wordsPL_EN.put(polishWord, translation);
//        wordsEN_PL.put(translations[0], translation);
//    }
//
//    public Optional<Tra> findTranslation(String word) {
//        word = word.toLowerCase();
//
//        if (wordsEN_PL.containsKey(word)) {
//            return Optional.ofNullable(wordsEN_PL.get(word));
//        } else if (wordsPL_EN.containsKey(word)) {
//            return Optional.ofNullable(wordsPL_EN.get(word));
//        }
//
//        return Optional.empty();
//    }
//
//}
