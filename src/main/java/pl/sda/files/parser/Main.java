package pl.sda.files.parser;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Dictionary2 slownik = new Dictionary2();

        String komenda;
        do {
            komenda = scanner.nextLine();

            String[] slowa = komenda.split(" ");
            switch (slowa[0]) {
                case "dodaj":
                    Language jezykZ = Language.valueOf(slowa[1]); // jezyk1
                    Language jezykNa = Language.valueOf(slowa[3]); // jezyk1

                    String slowo = slowa[2];
                    String tlumaczenie = slowa[4];

                    slownik.dodajTlumaczenie(jezykZ, slowo, new LanguageTranslation(jezykNa, tlumaczenie));
                    break;
                case "tlumacz":
                    String doTłumaczenia = slowa[1];

                    slownik.znajdzTlumaczenie(doTłumaczenia);
//                    if (translation.isPresent()) {
//                        System.out.println("Tłumaczenie: " + translation.get());
//                    } else {
//                        System.out.println("Nie znaleziono tłumaczenia!");
//                    }
                    break;
                case "quit":
                    break;
                default:
                    System.out.println("Nie rozpoznaje komendy");
            }
        } while (!komenda.equalsIgnoreCase("quit"));
    }
}
