package pl.sda.files.parser;

public enum Language {
    EN, PL, ES, DE
}
