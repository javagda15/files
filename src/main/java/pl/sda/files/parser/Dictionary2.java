package pl.sda.files.parser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Dictionary2 {
    private Map<Language, Map<String, Translation>> languageMap = new HashMap<>();

    public void dodajTlumaczenie(Language language, String word, LanguageTranslation languageTranslation) {
        dodajTlumaczenieW1Strone(language, word, languageTranslation);
        dodajTlumaczenieW1Strone(languageTranslation.getLanguage(), languageTranslation.getWord(), new LanguageTranslation(language, word));
    }

    public void dodajTlumaczenieW1Strone(Language language, String word, LanguageTranslation languageTranslation) {
        Map<String, Translation> wordsMap;
        if (languageMap.containsKey(language)) {
            wordsMap = languageMap.get(language);
        } else {
            wordsMap = new HashMap<>();
        }

        Translation translation;
        if (wordsMap.containsKey(word)) {
            translation = wordsMap.get(word);
            translation.addTranslation(languageTranslation.getLanguage(), languageTranslation.getWord());
        } else {
            translation = new Translation(languageTranslation.getLanguage(), languageTranslation.getWord());
        }
        wordsMap.put(word, translation);
        languageMap.put(language, wordsMap);
    }

    public void znajdzTlumaczenie(String word) {
        List<Map<String, Translation>> translations = languageMap
                .values()
                .stream()
                .filter(map -> map.containsKey(word))
                .collect(Collectors.toList());

        for (Map<String, Translation> map : translations) {
            System.out.println(map.get(word));
        }
    }

    public void znajdzTlumaczenie(String word, Language language) {

    }
}
