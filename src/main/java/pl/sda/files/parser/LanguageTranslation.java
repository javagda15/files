package pl.sda.files.parser;

public class LanguageTranslation {
    private Language language;
    private String word;

    public LanguageTranslation(Language language, String word) {
        this.language = language;
        this.word = word;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    @Override
    public String toString() {
        return "LanguageTranslation{" +
                "language=" + language +
                ", word='" + word + '\'' +
                '}';
    }
}
