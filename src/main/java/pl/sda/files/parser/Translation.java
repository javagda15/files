package pl.sda.files.parser;

import java.util.HashMap;
import java.util.Map;

public class Translation {
    private Map<Language, String> translations = new HashMap<>();

    public Translation(Language lang, String translation) {
        translations.put(lang, translation);
    }

    public void addTranslation(Language lang, String trans) {
        translations.put(lang, trans);
    }

    @Override
    public String toString() {
        return "Translation{" +
                "translation=" + translations +
                '}';
    }
}
