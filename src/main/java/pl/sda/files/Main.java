package pl.sda.files;

import java.io.File;

public class Main {
    public static void main(String[] args) {

        // deskryptor pliku
        File plik = new File("plik.txt");

        if(!plik.exists()){
            System.out.println("nie istnieje!");
        }

        if(plik.isFile()){
            System.out.println("jest plikiem");
        }

        if(plik.isDirectory()){
            System.out.println("jest katalogiem");
        }

        System.out.println(plik.length()); // rozmiar pliku
        System.out.println(plik.lastModified()); // czas modyfikacji

    }
}
